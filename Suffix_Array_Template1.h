#include<iostream>
#include<vector>
using namespace std;

template<class T>
vector<unsigned> radix_sort_suffix_array(const vector<unsigned>& G, const basic_string<T>& s);
template<class T>
vector<unsigned> bucket_sort_suffix_array(const vector<unsigned>& v,const basic_string<T>& s,unsigned cont);
template<class T>
vector<unsigned> ordinamento_G0_suffix_array(const vector<unsigned>& G0, const vector<unsigned>& G12_inverso, const basic_string<T>& s);
template<class T>
vector<unsigned> merge_suffix_array(const vector<unsigned>& G0, const vector<unsigned>& G12, const vector<unsigned> G12_inverso,const basic_string<T>& s);

//*************************************************************************************	
template<class T>
vector<unsigned> suffix_array(const basic_string<T>& input){
	unsigned lunghezza_input = input.length();

	if(lunghezza_input <= 0)
		return vector<unsigned>();
	
	if(lunghezza_input == 1)
		return vector<unsigned>(1,0);
	
	vector<unsigned> G0;
	vector<unsigned> G1;
	vector<unsigned> G2;

	// punto A
	for(unsigned i = 0; i < lunghezza_input; ++i)
		if( (i%3) == 0 )
			G0.push_back(i);
		else if( (i%3) == 1)
			G1.push_back(i);
		else
			G2.push_back(i);

	// punto B
	// string tris(string ,unsigned)
	// non effettivamente implementato: incorporato al PUNTO C.

	// punto C
	//implementate le funzioni:

	vector<unsigned> G12 = G1;
	

	for(unsigned i =0; i< G2.size(); ++i)
		G12.push_back(G2[i]);

	//utilizzato nel PUNTO E	
	vector<unsigned> G12_old  = G12;

	G12 = radix_sort_suffix_array(G12,input);

	//punto D
	T token = 0;
	basic_string<T> stringa_token;
	stringa_token.push_back(token);
	bool ricorsione_necessaria = false;

	for(unsigned i=1;i<G12.size(); ++i) {
		basic_string<T> sub_str_prec = input.substr(G12[i-1],3);
		basic_string<T> sub_str_attuale = input.substr(G12[i],3);

		++token;
		if( sub_str_prec == sub_str_attuale ){
			--token; // questo permette al token di rimanere invariato
			ricorsione_necessaria = true;
		}
		stringa_token = stringa_token + token;
	}


	// utilizzato da PUNTO E e PUNTO F
	// G12_inverso fornisce la posizione degli elementi di G12 
	// (ne e' quindi l'inverso perchè G12 fornisce l'elemento in una certa posizione)
	vector<unsigned> G12_inverso(input.length(),0);
	for(unsigned i = 0; i < G12.size(); ++i)
		G12_inverso[G12[i]] = i;



	// PUNTO E
	// FASE RICORSIVA.
	// la stringa_token è gia' ordinata.
	// se tutti i simboli sono diversi, allora è anche ordinata secondo i suffissi;
	// ma se sono presenti simboli uguali potrebbe essere necessario riordinarli:
	// si riodina tramite ricorsione.
	if(ricorsione_necessaria == true) {


		basic_string<T> stringa_ricorsiva;
		for(unsigned i = 0; i < G12.size(); ++i){
			T c = stringa_token[G12_inverso[G12_old[i]]]; // nebuloso: da spiegare...
			stringa_ricorsiva.push_back(c);
		}
		//DEBUG
//		cout << "ricorsione con: " << stringa_ricorsiva << endl;

		//chiamata ricorsiva
		vector<unsigned> G12_ordinato = suffix_array(stringa_ricorsiva); // da spiegare
		for(unsigned i = 0; i < G12.size(); ++i)
			G12[i] = G12_old[G12_ordinato[i]];
		
		//aggiorno il vettore G12_inverso.
		G12_inverso = vector<unsigned>(G12_inverso.size(),0);
		for(unsigned i = 0; i < G12.size(); ++i)
			G12_inverso[G12[i]] = i;


	}// fine ricorsione





	//PUNTO F
	//ordino G0 
	// secondo il carattere dei vari G0[i]
	// e, se parimerito, secondo la posizione di G0[i]+1 in G12. 
	G0 = ordinamento_G0_suffix_array(G0,G12_inverso,input);



	//PUNTO G
	vector<unsigned> result;
	result = merge_suffix_array(G0,G12,G12_inverso,input);

	//PUNTO H
	return result;
	// da sistemare:
	//...
	}

//*************************************************************************************	
// punto C
template<class T>
vector<unsigned> radix_sort_suffix_array(const vector<unsigned>& G, const basic_string<T>& s){

	vector<unsigned> result = G;
	
/*	sostituito da riga precedente
	//vector<string> g;
	for(unsigned j=0; j< G.size(); ++j)
		result.push_back(G[j]);		
*/
	for(unsigned i=3; i > 0 ; --i) 
		result = bucket_sort_suffix_array(result, s, i-1);	

	return result;
}


//*************************************************************************************	
//*********bucket_sort degli indici relativi a caratteri nella stringa*****************
template<class T>
vector<unsigned> bucket_sort_suffix_array(const vector<unsigned>& v, const basic_string<T>& s, unsigned cont) {

	vector<unsigned> result;	

	// DA FARE!!
	// ricercare minimo e massimo per sostituire:
	unsigned bucket_num = 'z'-'a' + 2;
	vector<unsigned> bucket[bucket_num]; 	

	for(unsigned i=0; i < v.size(); ++i) {
		if( (v[i]+cont) >= s.size() ){
			bucket[0].push_back(v[i]);
		}
		else{
			T char_attuale = s[v[i] + cont]; 		// prendo il carattere corrispondente all'i-esimo indice nel vettore.
			unsigned bucket_index = char_attuale -'a'+1;  	// calcolo l'indice del bucket relativo al carattere.
			bucket[bucket_index].push_back(v[i]);       // inserisco il carattere in coda alla lista del bucket.
		}	  			
	}

//	DEBUG:
//cout << "estrazione"<<endl;
	
	for(unsigned i = 0; i < bucket_num; ++i) 					//estraggo ordinatamente da tutti i bucket.
		for(unsigned j = 0; j < bucket[i].size(); ++j) {
//			cout << "estrazione: [" << (char)('a' +i -1) <<","<<j<<"]" <<endl; 
			unsigned index = bucket[i][j];
			result.push_back(index);				
	    }	
		
	return result;
}   

//*************************************************************************************	
template<class T>
vector<unsigned> ordinamento_G0_suffix_array(const vector<unsigned>& G0, const vector<unsigned>& G12_inverso, const basic_string<T>& s){


	// calcolo minimo e massimo per 
	unsigned min_ind = G12_inverso[G0[0] + 1]; 
	unsigned max_ind = G12_inverso[G0[0] + 1]; 

	//problema di costo computazionale?	(...sembrerebbe di no)
	for(unsigned i=1; i<G0.size(); ++i){
		if(G12_inverso[G0[i] + 1] < min_ind)
			min_ind = G12_inverso[G0[i] + 1];
		if(G12_inverso[G0[i] + 1] > max_ind)
			max_ind = G12_inverso[G0[i] + 1];
	}

	unsigned numero_bucket = max_ind - min_ind + 2;
	vector<unsigned> bucket2[numero_bucket];


/* DEBUG
	for(unsigned i=0; i<G0.size(); ++i)
		cout << G0[i] << endl;
*/

	//assegno ai bucket (al massimo uno per ogni bucket perchè sono tutti diversi)
	for(unsigned i=0; i<G0.size(); ++i){
		if((G0[i] +1) >= s.size())
			bucket2[0].push_back(G0[i]);
		else 
			bucket2[G12_inverso[G0[i]+1] - min_ind + 1].push_back(G0[i]);
	}

	//estraggo ordinatamente dai bucket
	vector<unsigned> result;
	for(unsigned i=0; i<numero_bucket; ++i) {
		while (!bucket2[i].empty()) {
			unsigned index = bucket2[i].back();
			result.push_back(index);
			bucket2[i].pop_back();
			if(!bucket2[i].empty())
				cout <<  "Trovati indici uguali dove richiesto siano diversi." << endl;
		}
	}

	result = bucket_sort_suffix_array(result,s,0);

	return result;
}
//*************************************************************************************	
//* 
//* Implementazione del merge finale tra i due insiemi di indici
//* NB: l'implementazione è diversa da quella originale
template<class T>
vector<unsigned> merge_suffix_array(const vector<unsigned>& G0, const vector<unsigned>& G12, const vector<unsigned> G12_inverso,const basic_string<T>& s){

/* // DEBUG
	for(unsigned j=0; j< G0.size(); ++j)
		cout << "G0["<<j <<"]=" <<G0[j] << endl;
	for(unsigned j=0; j< G12.size(); ++j)
		cout << "G12["<<j <<"]="<< G12[j] << endl;
/**/

	unsigned lunghezza_result = G0.size() + G12.size();
	vector<unsigned> result(lunghezza_result,0);

	unsigned p0 = 0;
	unsigned p12 = 0;

	for(unsigned i = 0; i < result.size(); ++i) {
		// caso: un vettore e' vuoto
		if( p0 >= G0.size() )
			result[i] = G12[p12++];
		else if( p12 >= G12.size() )
			result[i] = G0[p0++];
		//caso 1 e 2 
		else if(s.substr(G0[p0],3) < s.substr(G12[p12],3) )
			result[i] = G0[p0++];
		else if(s.substr(G12[p12],3) < s.substr(G0[p0],3) )
			result[i] = G12[p12++];
		//caso 3:
		else if( (G12[p12]%3) == 1 )
			{
			if( G12_inverso[G0[p0]+ 1] < G12_inverso[G12[p12] + 1] )
				result[i] = G0[p0++];
			else if( G12_inverso[G0[p0]+ 1] > G12_inverso[G12[p12] + 1] )
				result[i] = G12[p12++];
			else 
				{
				cout << "non gestito mod1 per stringa: "<< s << endl;
				break;					
				}				
			}
		else
			{
			if( G12_inverso[G0[p0]+ 2] < G12_inverso[G12[p12] + 2] )
				result[i] = G0[p0++];
			else if( G12_inverso[G0[p0]+ 2] > G12_inverso[G12[p12] + 2] )
				result[i] = G12[p12++];
			else 
				{
				cout << "non gestito mod2 per stringa: "<< s << endl;
				cout << G0[p0]<< "," << G12[p12] << endl;
				cout << s.substr(p0,3) <<","<< s.substr(p12,3) << endl; 
				break;					
				}				

			}
	}


	return result;

}
